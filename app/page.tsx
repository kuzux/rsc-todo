import Login from "@/components/login";
import { TodoList } from "@/components/todo_list";
import { verifyToken } from "@/lib/auth";
import { TodoItem } from "@/lib/todo_item";
import { cookies } from "next/headers";

export default async function Home() {
    let token = cookies().get("token")?.value;
    let loggedIn = false;
    if(token) {
        let userInfo = await verifyToken(token);
        loggedIn = !!userInfo?.userId;
    }

    /* this is a server component, executes only on the server side
     * and then sends a bunch of html (and the client component js) to the client
     * we can use async functions here, but we can't use react hooks (for client side interactivity)
     * so we call the async server side function, and then pass the data we get from it to a client component */
    if(!loggedIn)
        return <main><Login /></main>;

    let items: TodoItem[] = await TodoItem.getItems();
    return <main>
        <TodoList items={items} />
    </main>;
}
