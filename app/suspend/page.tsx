"use client"

import { Button } from "@/components/ui/button";
import Show from "@/components/ui/show";
import { getTodoItemCount } from "@/lib/actions";
import { useEffect, useState, useTransition } from "react";

function DisplayTodoCount() {
    // isLoading changing does not trigger a rerender, which is either a bug or plain annoying
    let [isLoading, startTransition] = useTransition();
    let [itemCount, setItemCount] = useState(-1);

    /* What we end up doing here is just call a server action that returns the data as a js object
     * Not ideal because we issue a post request from the client, not a get request
     * But it works and we don't need to deal with json serialization/deserialization
     * Plus we get type safety. So, positive in total :)
     * Hoping to get server actions as http get requests one day */
    useEffect(() => startTransition(async () => {
        console.log("Waiting on the client...");
        let count = await getTodoItemCount();
        await new Promise(resolve => setTimeout(resolve, 5000));
        console.log({ count });
        setItemCount(count);
        console.log("Done waiting on the client!");
    }), []);

    if(isLoading) return <p className="text-yellow-800">Loading...</p>;

    return <p className="text-green-800">New data from the Server!, There are {itemCount} todo items</p>
}

export default function SuspendPage() {
    let [count, setCount] = useState(0);

    return <div className="flex flex-col">
        <Show when={count >= 3}>
            <DisplayTodoCount />
        </Show>
        <p>current count is {count}</p>
        <Button onClick={() => setCount(count + 1)}>Increment</Button>
    </div>
}