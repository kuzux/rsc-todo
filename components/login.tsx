"use client";

import Show from "@/components/ui/show";
import { Alert } from "@/components/ui/alert";
import { Button } from "@/components/ui/button";
import { Input } from "@/components/ui/input";
import { doLogin, doSignup } from "@/lib/actions";
import { useState, useTransition } from "react";
import { redirect } from "next/navigation";

export default function Login() {
    let [username, setUsername] = useState("");
    let [password, setPassword] = useState("");
    let [action, setAction] = useState<"login"|"signup">("login");
    let [error, setError] = useState<string|null>(null);
    let [isPending, startTransition] = useTransition();

    let handleLogin = async (action: "login"|"signup", username: string, password: string) => {
        await startTransition(async () => {
            let result = (action == "login") ? 
                await doLogin(username, password)
                : await doSignup(username, password);
            if(result.result == "success") {
                redirect("/");
            } else {
                setError(result.error);
            }
        });
    }

    return <form onSubmit={(e) => {
        e.preventDefault();
        handleLogin(action, username, password);
    }} className="flex flex-col gap-4">
        <Show when={!!error}>
            <Alert className="mb-6" variant="destructive">{error}</Alert>
        </Show>
        <p>
            <label htmlFor="username">Username</label>
            <Input value={username} onChange={e => setUsername(e.target.value)} type="text" name="username" placeholder="johndoe" />
        </p>
        <p>
            <label htmlFor="password">Password</label>
            <Input value={password} onChange={e => setPassword(e.target.value)} type="password" name="password" placeholder="********" />
        </p>
        <div className="flex gap-2">
            <Button type="submit" onClick={() => setAction("login")}>Login</Button>
            <Button type="submit" onClick={() => setAction("signup")}>Signup</Button>
        </div>
    </form>
}
