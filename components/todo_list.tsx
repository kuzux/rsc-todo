"use client";
/* we need to put this to mark it as a client component (where we can use react hooks)
 * server components => nothing
 * client components => "use client"
 * server actions => "use server" */

import { useState, useTransition } from "react";

// we can import types safewly from server side files
import { TodoItem } from "@/lib/todo_item";
import { addTodoItem, doLogout } from "@/lib/actions";

/* the ui components are from shad ui, 
 * they use tailwind css, configured in app/globals.css */
import { Input } from "./ui/input";
import { Button } from "./ui/button";
import { redirect } from "next/navigation";

export function TodoList({ items }: { items: TodoItem[] }) {
    /* the "items" prop comes from the server on the initial render,
     * but we want to be able to add items to the list later without reloading the page */
    let [displayedItems, setDisplayedItems] = useState(items);
    /* we have to put the call to react server action inside the startTransition call
     * we can just call a server action (defined in a separate file with "use server")
     * from an on click handler like that */
    let [isPending, startTransition] = useTransition();
    // this just holds the value of the input field, not sent to the server yet
    let [nameToAdd, setNameToAdd] = useState("");


    return <>
        <ul>
            {displayedItems.map(item => <li key={item.id}>{item.text}</li>)}
        </ul>
        <p className="flex gap-2">
            {}
            <Input type="text" placeholder="Name" onChange={e => setNameToAdd(e.target.value)} />
            <Button onClick={() => startTransition(async () => {
                let todoItem = await addTodoItem(nameToAdd);
                setDisplayedItems([...displayedItems, todoItem]);
            })}>Add</Button>
        </p>
        <p className="mt-12">
            <Button variant="link" onClick={() => startTransition(async () => {
                doLogout();
                redirect("/");
            })}>Logout</Button>
        </p>
    </>
}
