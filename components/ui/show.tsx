// this one's not from shad ui, just a helper for conditional rendering
type ShowProps = {
    when: boolean,
    children: React.ReactNode,
    fallback?: React.ReactNode
};

export default function Show({ when, children, fallback }: ShowProps) {
    return when ? <>{children}</> : (fallback ?? null);
}