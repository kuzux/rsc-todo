"use server";

import { cookies } from 'next/headers'
import { TodoItem } from "@/lib/todo_item";
import { SignupResult, createToken, createUser, verifyLogin } from '@/lib/auth';

// this is the action to call from the client component
// just adds a todo item and returns it
export async function addTodoItem(text: string): Promise<TodoItem> {
    console.log("Adding todo item", text);
    let item = await TodoItem.addItem(text);
    return item;
}

export type LoginResult = 
    { result: "success", token: string } 
    | { result: "failure", error: string };

export async function doLogin(username: string, password: string): Promise<LoginResult> {    
    console.log("Logging in", username, password);
    let canLogin = await verifyLogin(username, password);
    if (!canLogin) {
        return { result: "failure", error: "Invalid username or password" };
    }
    let jwt = await createToken({ username, userId: "123" });
    cookies().set("token", jwt);
    return { result: "success", token: jwt };
}

export async function doLogout(): Promise<void> {
    console.log("Logging out");
    cookies().delete("token");
}

export async function doSignup(username: string, password: string): Promise<SignupResult> {
    console.log("Signing up");
    return createUser(username, password);
}

export async function getTodoItemCount(): Promise<number> {
    return TodoItem.numberOfItems();
}