import { SignJWT, jwtVerify } from 'jose'
import { PrismaClient } from '@prisma/client'
import bcrypt from 'bcrypt';

let secret = new TextEncoder().encode(process.env.JWT_SECRET);
let alg = 'HS256';

let prisma = new PrismaClient();

export type TokenData = {
    username: string;
    userId: string;
};

export async function createToken(payload: TokenData): Promise<string> {
    let jwt = await new SignJWT(payload)
        .setProtectedHeader({ alg })
        .setIssuedAt()
        .setExpirationTime('12h')
        .sign(secret);
    return jwt;
}

export async function verifyToken(token?: string): Promise<TokenData | null> {
    if(!token) return null;
    
    try {
        let { payload } = await jwtVerify(token, secret, { algorithms: [alg] });
        return { username: payload?.username as string, userId: payload?.userId as string };
    } catch (e) {
        return null;
    }
}

export async function verifyLogin(username: string, password: string): Promise<boolean> {
    let user = await prisma.user.findUnique({ where: { username } });
    if(!user) return false;
    let result = await bcrypt.compare(password, user.hashedPassword);
    
    return result;
}

export type SignupResult =
    { result: "success", userId: string }
    | { result: "failure", error: string };

export async function createUser(username: string, password: string): Promise<SignupResult> {
    let hashedPassword = await bcrypt.hash(password, 10);
    try {
        let user = await prisma.user.create({ data: { username, hashedPassword } });
        return { result: "success", userId: user.id };
    } catch (e: any) {
        return { result: "failure", error: e.message };
    }
}
