/* this file also runs on the server
 * it is not directly called from a client component, only via server actions/ server components */
import { PrismaClient } from '@prisma/client'

/* we use prisma as the db client here
 * it is configured by prisma/schema.prisma
 * it creates a bunch of db access functions for us, which is nice
 * it takes and returns plain js objects */
let prisma = new PrismaClient();

/* this type of definition is for "narrowing" plain js objects
* what is taken/returned by prisma "conforms" to this definition */
export type TodoItem = {
    id: string;
    text: string;
}

/* we can define a type and a module with the same name
 * we can then use the functions like TodoItem.getItems() or TodoItem.addItem() */
export module TodoItem {
    export async function getItems(): Promise<TodoItem[]> {
        return prisma.todoItem.findMany();
    }

    export async function addItem(text: string): Promise<TodoItem> {
        let newItem = await prisma.todoItem.create({ data: { text } });
        return newItem;
    }

    export async function numberOfItems(): Promise<number> {
        console.log("Getting todo item count");
        let count = await prisma.todoItem.count();
        return count;
    }
}
