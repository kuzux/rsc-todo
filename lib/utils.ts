import { type ClassValue, clsx } from "clsx"
import { twMerge } from "tailwind-merge"
 
export function cn(...inputs: ClassValue[]) {
    // added by shad-ui, merges class names
    return twMerge(clsx(inputs))
}
