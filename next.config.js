/** @type {import('next').NextConfig} */
const nextConfig = {
    // these are needed for using server actions
    experimental: {
        serverActions: true,
        serverActionsBodySizeLimit: '2mb',
    }
};

module.exports = nextConfig
