import { createToken, verifyToken } from "@/lib/auth";

describe('auth.createToken and auth.verifyToken', () => {
    test('they should create something that looks like a signed jwt', async () => {
        let jwt = await createToken({ username: "test", userId: "123" });
        expect(jwt).toMatch(/^[a-zA-Z0-9_-]+\.[a-zA-Z0-9_-]+\.[a-zA-Z0-9_-]+$/);
    });

    test('they should have a 12 hour expiration', async () => {
        let jwt = await createToken({ username: "test", userId: "123" });
        let parts = jwt.split(".");
        let payload = JSON.parse(atob(parts[1]));
        expect(payload.exp - payload.iat).toEqual(12 * 60 * 60);
    });

    test('they should create something that gets verified', async () => {
        let jwt = await createToken({ username: "test", userId: "123" });
        let userInfo = await verifyToken(jwt);
        expect(userInfo).toEqual({ username: "test", userId: "123" });
    });

    test('they should not verify a null token', async () => {
        let userInfo = await verifyToken(undefined);
        expect(userInfo).toEqual(null);
    });

    test('they should not verify a token with bad signature', async () => {
        let jwt = await createToken({ username: "test", userId: "123" });
        let badJwt = `${jwt}badsignature`;
        let userInfo = await verifyToken(badJwt);
        expect(userInfo).toEqual(null);
    });

    test('they should not verify a token with an invalid format', async () => {
        let jwt = "not-a-valid-token";
        let userInfo = await verifyToken(jwt);
        expect(userInfo).toEqual(null);
    });
})
